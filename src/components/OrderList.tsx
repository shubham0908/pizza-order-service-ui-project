import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Order } from './Order';
import { OrderPizza } from './OrderPizza';
import { Link } from 'react-router-dom';

const OrderList: React.FC = () => {
  const [orderItems, setOrderItems] = useState<Order[]>([]);
  const [editingOrder, setEditingOrder] = useState<Order | null>(null);
  const [editedSize, setEditedSize] = useState('');
  const [editedQuantity, setEditedQuantity] = useState(0);
  const [editedDeliveryAddress, setEditedDeliveryAddress] = useState('');
  const [editedTotalPrice, setEditedTotalPrice] = useState(0);

  const [availableSizes, setAvailableSizes] = useState<string[]>(['Regular', 'Medium', 'Large']);



  useEffect(() => {
    fetchOrders();
  }, []);

  const fetchOrders = async () => {
    try {
      const response = await axios.get('http://localhost:8080/orders');
      const data = response.data.data;

      if (Array.isArray(data)) {
        const sortByOrderId = response.data.data.sort((a: { orderId: number; }, b: { orderId: number; }) => b.orderId - a.orderId).slice(0, 1);        
        setOrderItems(sortByOrderId);
      } else {
        console.error('Invalid data format');
      }
    } catch (error) {
      console.error('Error fetching orders:', error);
    }
  };

  const handleEditOrder = (order: Order) => {
    setEditingOrder(order);
    setEditedSize(order.pizzas[0]?.size || '');
    setEditedQuantity(order.pizzas[0]?.quantity || 0);
    setEditedDeliveryAddress(order.deliveryAddress);
    // const singlePizzaPrice = order.pizzas[0]?.quantity || 0;
    // console.log('Price of First Pizza:', order.pizzas[0]?.price);
    // console.log('Edited quantity:', order.pizzas[0]?.quantity || 0);

   // const totalPrice =(order.pizzas[0]?.price)*(order.pizzas[0]?.quantity || 0);
    
 // setEditedTotalPrice((order.pizzas[0]?.price)*(order.pizzas[0]?.quantity || 0));
  //console.log('total price',totalPrice);

  //   setEditedTotalPrice({
  //  order:order.pizzas.map((prevItem)=>({

  //     price:prevItem.price*editedQuantity

  //   }))
  // });
  const singlePizzaPrice=(order.pizzas[0]?.price)/(order.pizzas[0]?.quantity || 0);

  console.log("single pizza price",singlePizzaPrice);
 // const totalPrice=singlePizzaPrice*order.pizzas[0].quantity;
  setEditedTotalPrice(singlePizzaPrice*order.pizzas[0].quantity);
  
  };

  const handleUpdateOrder = async () => {
    try {
      if (!editingOrder) {
        return;
      }

     
      const updatedOrder: Order = {
        ...editingOrder,
        deliveryAddress: editedDeliveryAddress,
        pizzas: editingOrder.pizzas.map((pizza) => ({
          ...pizza,
          size: editedSize,
          quantity: editedQuantity,
          price:editedTotalPrice
        })),
      };

      await axios.put(`http://localhost:8080/orders/${editingOrder.orderId}`, updatedOrder);

      setOrderItems((prevOrders) =>
        prevOrders.map((order) => (order.orderId === editingOrder.orderId ? updatedOrder : order))
      );

      setEditingOrder(null);
    } catch (error) {
      console.error('Error updating order:', error);
    }
  };
 

  return (
    <div style={{ width: '90%', margin: 'auto', padding: '0px' }}>
      <h1 style={{ textAlign: 'center', fontSize: '24px', margin: '20px 0' }}>Order List</h1>
      <table style={{ borderCollapse: 'collapse', width: '100%' }}>
        <thead>
          <tr style={{ borderBottom: '1px solid #ccc', background: '#f2f2f2' }}>
            <th>Order ID</th>
            <th>Delivery Address</th>
            <th>Status</th>
            <th>Total Amount</th>
            <th>Pizzas</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {orderItems.map((order: Order) => (
            <tr key={order.orderId} style={{ borderBottom: '1px solid #ccc' }}>
              <td>{order.orderId}</td>
              <td>{order.deliveryAddress}</td>
              <td>{order.status}</td>
              <td>${order.totalAmount}</td>
              <td>
                <ul>
                  {order.pizzas.map((pizza: OrderPizza) => (
                    <li key={pizza.pizzaId}>
                      <p>Size: {pizza.size}</p>
                      <p>Quantity: {pizza.quantity}</p>
                      <p>SubTotal: ${pizza.price}</p>
                    </li>
                  ))}
                </ul>
              </td>
              <td>
                {editingOrder?.orderId === order.orderId ? (
                  <>
                    <input type="text" value={editedDeliveryAddress} onChange={(e) => setEditedDeliveryAddress(e.target.value)} />
                    <br />
                    <br />
                    <select value={editedSize} onChange={(e) => setEditedSize(e.target.value)}>
                      {availableSizes.map((size) => (
                        <option key={size} value={size}>
                          {size}
                        </option>
                      ))}
                    </select>{' '}
                    <br />
                    <br />
                    <input type="number" value={editedQuantity} onChange={(e) => setEditedQuantity(Number(e.target.value))} />
                    <br />
                    <br />
                    <button onClick={handleUpdateOrder}>Confirm Order</button>
                  </>
                ) : (
                  <><Link to={"/"}>
                    <button >Update</button>
                    </Link>
                  </>
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default OrderList;
