import React, { useState } from 'react';
import { Button, Card, ListGroup } from 'react-bootstrap';
import { Pizza } from '../pages/Pizza';

interface ProductProps {
  pizza: Pizza;
  addToCart: (pizza: Pizza, size: string,quantity:number,price:number) => void;
}

const Product: React.FC<ProductProps> = ({ pizza, addToCart }) => {
  const [selectedSize, setSelectedSize] = useState('');
  const [quantity, setQuantity] = useState(1);
  const [selectedPrice, setSelectedPrice] = useState(0);

  const handleAddToCart = () => {
    if(selectedSize){    
    addToCart(pizza, selectedSize,quantity,selectedPrice);
    setSelectedSize('');
    setSelectedPrice(0);
  }
  else{
    alert('Please select a size before adding to cart.');
  }

  };

  // const handleQuantityChange = (event: React.ChangeEvent<HTMLInputElement>) => {
  //   console.log(event.target.value)
  //   const value = Number(event.target.value);
  //   setQuantity(value);
  //   };


  const handleSizeSelection = (event: React.ChangeEvent<HTMLSelectElement>) => {
   console.log(event.target.value)
    setSelectedSize(event.target.value);
    switch (event.target.value) {
      case 'Regular':
        setSelectedPrice(pizza.priceRegularSize);
        break;
      case 'Medium':
        setSelectedPrice(pizza.priceMediumSize);
        break;
      case 'Large':
        setSelectedPrice(pizza.priceLargeSize);
        break;
      default:
        setSelectedPrice(0);
        break;
    }
  };



  return (
    <Card style={{ width: '19rem', margin: '10px' }}>
      <Card.Img variant="top" src={pizza.imageUrl} />
      <Card.Body>
      <ListGroup className="list-group list-group-flush" style={{position:'sticky' ,paddingTop:'10px'}}>
           
           
              <ListGroup.Item as="li"> <Card.Title>{pizza.name}</Card.Title></ListGroup.Item>
              <ListGroup.Item as="li"> <Card.Text>{pizza.description}</Card.Text></ListGroup.Item>
        <div >
         <p className='pt-3'>Select Size:</p>
          
            {/* <Dropdown.Toggle variant="primary" id="size-dropdown">
              {selectedSize ? `${selectedSize} - $${selectedPrice}` : 'Select Size'}
            </Dropdown.Toggle> */}
            <select className='mb-4' value={selectedSize}  onChange={handleSizeSelection} >
            <option value="" >Size  </option>
              <option  value="Regular"  >Regular - ${pizza.priceRegularSize}</option>
              <option   value="Medium"  >Medium - ${pizza.priceMediumSize}</option>
              <option  value="Large" >Large - ${pizza.priceLargeSize}</option>
            </select>
          
        </div>
        {/* <div>
        <p>Quantity:</p>
        <input type="number" value={quantity} min={1} onChange={handleQuantityChange} />
        </div> */}
        <Button   variant="primary"  onClick={handleAddToCart}   >
          Add to Cart
        </Button>
        </ListGroup>
      </Card.Body>
    </Card>
  );
};

export default Product;
