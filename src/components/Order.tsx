

import { OrderPizza } from "./OrderPizza";

export interface Order {
    customerId: number;
    orderId?: number;
    status?: string;
    totalAmount: number;
    orderDateTime?:string;
    deliveryAddress:string;
    pizzas: OrderPizza[];
  }