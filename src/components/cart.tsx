import React from 'react';
import { Button, Card, ListGroup ,Table} from 'react-bootstrap';
import { Pizza } from '../pages/Pizza';
import  { useEffect, useState } from 'react';
import axios from 'axios';
import { Order } from './Order';
import { Link } from 'react-router-dom';
import { type } from '@testing-library/user-event/dist/type';


interface CartProps {
  cartItems: Pizza[];
  updateQuantity: (pizzaId: number, quantity: number,selectedSize:string) => void;
  clearCart: () => void;
 

}



const Cart: React.FC<CartProps> = ({ cartItems,updateQuantity,clearCart}) => {
  const [order, setOrder] = useState<Order>();

  
 




  const createOrder = async () => {
    
      // Make the POST request to create the order

     
      console.log(cartItems);

      try {
        const orders: Order = {
          customerId:7,
          //orderId: 1,
          //status: 'pending',
          totalAmount: calculateTotalAmount(),
         // orderDateTime: '123',
          deliveryAddress:'pune',
          pizzas: cartItems.map((item) => ({
           
            pizzaId: item.pizzaId,
            quantity: item.quantity,
            size: item.selectedSize,
            price: item.selectedPrice*item.quantity
          })),
        };
  
   
    

      console.log(orders);
      setOrder(orders);
      const response = await axios.post('http://localhost:8080/orders', orders);
     
      
     if(response){
      alert('Order created successfully'); 
      clearCart(); 
    
      window.location.href = '/cart';   
    }

    } catch (error) {
      console.error('Error creating order:', error);
    }
  };

  const handleCreateOrder = () => {
    if (cartItems.length > 0) {
      createOrder();
     
    }
    else  {
      alert('Please select a product before creating order.');
    }
  };


  const handleQuantityChange = (pizzaId: number, quantity: number,selectedSize:string) => {

   
  updateQuantity(pizzaId, quantity, selectedSize);
    
  };
  const calculateTotalAmount = () => {
    
    let subtotal = 0;
    cartItems.forEach((item) => {
      subtotal += item.selectedPrice * item.quantity;
    });
    return subtotal;
  };

 

 
  return (
    <Card style={{ position: 'fixed', height:'600px', top: '150px', right: '40px', width: '450px', maxHeight: 'calc(75vh - 150px)', overflowY: 'auto' ,border: '1px solid #ccc' }}>
      <Card.Body style={{ paddingBottom: "70px", position: "relative" }}>
        <Card.Title>Your Order</Card.Title>
        {cartItems.length === 0 ? (
          <Card.Text>
            <svg xmlns="http://www.w3.org/2000/svg" width="130" height="130" fill="currentColor" className="bi bi-cart" viewBox="0 0 16 16">
              <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
            </svg>
            <h5>Cart is empty. Add Products</h5>
          </Card.Text>
        ) : (
          <div>
               <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Size</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                {cartItems.map((item) => (
                  <tr key={item.pizzaId}>
                    <td>
                      <img src={item.imageUrl} alt={item.name} style={{ width: '80px' }} />
                    </td>
                    <td>{item.name}</td>
                    <td>{item.description}</td>
                    <td>{item.selectedSize}</td>
                    <td>
                      <Button variant="outline-primary" onClick={() => handleQuantityChange(item.pizzaId, item.quantity - 1, item.selectedSize)}>
                        -
                      </Button>
                      <span className="mx-2">{item.quantity}</span>
                      <Button variant="outline-primary" onClick={() => handleQuantityChange(item.pizzaId, item.quantity + 1, item.selectedSize)}>
                        +
                      </Button>
                    </td>
                    <td>${item.selectedPrice}</td>
                    <td>${item.selectedPrice * item.quantity}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <div  style={{position:'sticky'}}>
            <ListGroup className="list-group list-group-flush" >
              <ListGroup.Item as="li">Sub Total:{calculateTotalAmount()}</ListGroup.Item>
              {/* <ListGroup.Item as="li">Min. Order:</ListGroup.Item>
              <ListGroup.Item as="li">Delivery Charge:</ListGroup.Item>
              <ListGroup.Item as="li">I have a coupon:</ListGroup.Item> */}
            </ListGroup>
            </div>
          </div>
        )}
        <Button style={{ marginTop: '20px' }} variant="primary" onClick={handleCreateOrder}> 
          Create Order
        </Button>
      </Card.Body>
    </Card>
  );
};


export default Cart;
