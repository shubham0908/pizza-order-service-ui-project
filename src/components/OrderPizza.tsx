export interface OrderPizza {
    pizzaId: number;
    name?:string;
    quantity: number;
    size: string;
    price : number;
  }