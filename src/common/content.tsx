import {
    BrowserRouter,
    Routes,
    Route,
  } from "react-router-dom";
  import MainPage from "../pages/mainPage";
import MainCart from "../pages/mainCart";
import OrderList from "../components/OrderList";
const Content = () => {
    return(
    <>
        <BrowserRouter>
            <Routes>
                <Route path="/" Component={MainPage}>
                </Route>
                <Route path="/cart" Component={MainCart}>
                </Route>
                

            </Routes>
        </BrowserRouter>
    </>
    )
}

export default Content;