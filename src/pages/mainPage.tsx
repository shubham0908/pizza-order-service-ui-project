import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Card,Button } from 'react-bootstrap';
import Product from '../components/product';
import Cart from '../components/cart';
import { Order } from '../components/Order';
import axios from 'axios';
import { Pizza } from './Pizza';

const MainPage = () => {
  const [pizzas, setPizzas] = useState<Pizza[]>([]);
  const [cartItems, setCartItems] = useState<Pizza[]>([]);
  const [pizzaType, setPizzaType] = useState<'veg' | 'nonveg' | 'all'>('all'); // New state to track pizza type selection


  



  useEffect(() => {
    fetchPizzas();
  }, []);

  const clearCart = () => {
    setCartItems([]);
  };

  const fetchPizzas = async () => {
    try {
      const response = await axios.get('http://localhost:8080/pizzas');
      console.log(response.data.Data);

      setPizzas(response.data.Data);
    } catch (error) {
      console.error('Error while fetching pizzas:', error);
    }
  };

  const addToCart = (pizza: Pizza, selectedSize: string, quantity: number, selectedPrice: number) => {

    const existingPizza = cartItems.find((item) => item.pizzaId === pizza.pizzaId && item.selectedSize == selectedSize);

    if (existingPizza) {
      const updatedItems = cartItems.map((item) => {
        if (item.pizzaId === existingPizza.pizzaId && item.selectedSize === selectedSize) {
          return { ...item, quantity: item.quantity + quantity, selectedPrice, selectedSize: selectedSize || '' };
        }
        return item;
      });

      setCartItems(updatedItems);
    } else {
      const newPizza = { ...pizza, selectedSize: selectedSize || '', quantity, selectedPrice };

    setCartItems((prevItems) => [...prevItems, newPizza]);
    }

  };

  const updateQuantity = (pizzaId: number, quantity: number, selectedSize: string) => {
    if (quantity < 1) {
      removeCartItem(pizzaId, selectedSize);
    } else {
      setCartItems((prevItems) =>
        prevItems.map((item) =>
          item.pizzaId === pizzaId && item.selectedSize === selectedSize ? { ...item, quantity: quantity } : item
        )
      );
    }
    console.log(selectedSize);


  };


  const removeCartItem = (pizzaId: number, selectedSize: string) => {
    setCartItems((prevItems) => prevItems.filter((item) => item.pizzaId !== pizzaId || item.selectedSize !== selectedSize));

  };

  const filteredPizzas = pizzaType === 'all' ? pizzas : pizzas.filter(pizza => pizza.type === pizzaType);


 


  return (
    <Container fluid style={{ margin: 'auto', paddingTop: '60px', backgroundColor: '#EEE' }}>
      <Row>
      <Row>
            <Col>
              <Button variant="secondary" onClick={() => setPizzaType('all')}>
                All Pizzas
              </Button>
            </Col>
            <Col>
              <Button variant="success" onClick={() => setPizzaType('veg')}>
                Veg Pizza
              </Button>
            </Col>
            <Col >
              <Button variant="danger" onClick={() => setPizzaType('nonveg')}>
                Non-Veg Pizza
              </Button>
            </Col>
          </Row>
       
        <Col xs sm={8} lg={8} >
          <Card style={{ backgroundColor: 'lightgrey' }}>
            <Row>
              {filteredPizzas.map(pizza => (
                <Col key={pizza.pizzaId}>
                  <Product key={pizza.pizzaId} pizza={pizza} addToCart={addToCart} />
                </Col>
              ))}
            </Row>
          </Card>
        </Col>
        <Col xs sm={4} lg={4}>
          <Card style={{ backgroundColor: '#EEE' }}>
            <Cart cartItems={cartItems} updateQuantity={updateQuantity} clearCart={clearCart}  />
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default MainPage;
