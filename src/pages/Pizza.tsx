export interface Pizza {
    pizzaId: number;
    name: string;
    description: string;
    type: string;
    imageUrl: string;
    priceRegularSize: number;
    priceMediumSize: number;
    priceLargeSize:number;
    selectedSize:string;
    selectedPrice:number;
    // size:String;
    quantity: number; 
    //price:number;


  }